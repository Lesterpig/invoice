module lesterpig.com/invoice

go 1.13

require (
	github.com/jung-kurt/gofpdf v1.12.1
	gopkg.in/yaml.v2 v2.2.2
)
